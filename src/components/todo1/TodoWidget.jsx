/**
 * Created by zekar on 7/30/2016.
 */

import React, {Component} from 'react';
import {observable, action} from 'mobx';
import {observer} from 'mobx-react';
import DevTools from 'mobx-react-devtools';

import config from '../../app/config/config';

class Todo {
    id = "";
    @observable title = "";
    @observable description = "";
    @observable isComplete = false;

    constructor(id, title, description, isComplete) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isComplete = isComplete;
    }
}

class TodoList {
    constructor() {
        this.getAll();
    }

    @observable todos = [];
    @observable isLoading = true;

    @action getAll() {
        this.todos.length = 0;

        window.fetch(config.apiRootUrl + 'api/todo')
            .then(response => response.json())
            .then(data => {
                for(var item of data) {
                    this.todos.push(new Todo(item.Id, item.Title, item.Description, item.IsComplete));
                }
                this.isloading = false;
            })
    }

    @action add(todo, onSuccess) {
        var data = new FormData();
        data.append("title", todo.title);
        data.append("description", todo.description);

        window.fetch(config.apiRootUrl + 'api/todo', {
            method: 'POST',
            body: data
        }).then(() => {
            this.getAll();
            // this.todos.push(todo);
            onSuccess();
        });
    }

    @action remove(id) {
        window.fetch(config.apiRootUrl + 'api/todo/' + id, {
            method: 'DELETE'
        }).then(() => {
            this.todos = this.todos.filter(todo => todo.id !== id);
        });
    }
}

const TodoView = observer(({todo, handleDelete}) =>
    <li>
        <input
            type="checkbox"
            checked={todo.isComplete}
            onClick={() => todo.isComplete = !todo.isComplete}
        />{todo.title} {todo.id} <button onClick={e => handleDelete(todo.id)}>delete</button>
    </li>
);

@observer class TodoListView extends Component {
    @observable newTitle = "";
    @observable newDescription = "";

    render() {
        return (<div>
            <h3>New item</h3>
            Title: <input type="text" onChange={e => this.newTitle = e.target.value} /><br />
            Description: <input type="text" onChange={e => this.newDescription = e.target.value} /><br />
            <button onClick={e => this.handleAdd()} >Add {this.newTitle}</button>
            <hr />
            <h3>Items</h3>
            <ul>
                {this.props.isLoading ? "Loading..." : ""}
                {this.props.isLoading ? "" : this.props.todoList.todos.map(todo =>
                    <TodoView todo={todo} key={Math.random()} handleDelete={this.handleDelete} />
                )}
            </ul>
        </div>)
    }

    handleAdd = () => {
        const onSuccess = () => {
            this.newTitle = "";
            this.newDescription = "";
        };
        this.props.todoList.add(new Todo("", this.newTitle, this.newDescription, false), onSuccess);
    };

    handleDelete = (id) => {
        this.props.todoList.remove(id);
    };
}

const store = new TodoList();

class TodoWidget extends Component {
    render() {
        return (
            <div>
                <h3>Todos:</h3>
                <TodoListView todoList={store} />
                <DevTools />
            </div>
        )
    }
}

export default TodoWidget;